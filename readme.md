# Excel-Translator
### Highly efficient, hackable translator for big excel files

# Installation

`npm install @gembit/excel-translator`

# Usage
```javascript
import { parseStream, parseFile } from "@gembit/excel-translator";

const translator = {
    "*": {
        key: ({ key }) => {
            return key + "-new-key";
        }
    }
    "cena": {
        value: ({ value }) => {
            return (value * 1.23) + "PLN";
        }
    }
};

const parsed = parseStream(yourStream, translator);
```

# Example

```javascript
import { parseStream } from "@gembit/excel-translator";
import fs from "fs-extra";
import utf8 from "utf8";

const filePath = __dirname + "/files/testFileXML.xml";

const stream = fs.createReadStream(filePath);

const translator = {
    "*": {
        key: ({ key }) => {
            return utf8.decode(key);
        }
    },
    "ąbć11": {
        key: "oneone",
        value: ({ value }) => {
            return value * 2;
        }
    }
};

const parsedResults = await parseStream(fs.createReadStream(filePath), translator);

console.log("Correct: ", parsedResults.good);
console.log("Bad: ", parsedResults.bad);
```

## Possible Problems

`Empty _encoding` in `csv-stream` module. In order to fix it change the _encoding value from empty string to `undefined`(or `utf-8`) in main file od `csv-stream`