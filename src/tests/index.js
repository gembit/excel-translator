// @flow

import { expect } from "chai";
import { parseStream } from "../lib/";
import fs from "fs-extra";
import type { ParsedResults } from "../lib/";

describe("XML Importing", () => {
    it("Parse the given stream", async () => {
        const filePath = __dirname + "/files/testFileXML.xml";

        fs.createReadStream(filePath).pipe(process.stdout);

        const translator = {
            "*": {
                key: ({ key }) => {
                    return key;
                }
            },
            "ąbć11": {
                key: "oneone",
                value: ({ value }) => {
                    return value * 2;
                }
            }
        };
        
        const parsedResults: ParsedResults = await parseStream(fs.createReadStream(filePath, { encoding: "utf-8" }), translator);

        expect(parsedResults.good).to.not.be.empty;
    });
});