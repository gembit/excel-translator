// @flow

export type MapObject = {
    [key: string]: {
        key: Function | any,
        value: Function | any
    }
}

/**
 * Parses the `target` into a Model
 * @returns {Object} - Model
 * @param {Object} target 
 * @param {MapObject} map
 */
export function xmlObjectToModel(target: Object, map: MapObject): Object {
    const resultObject = {};
    
    const targetKeys = Object.keys(target);

    for (let targetKey of targetKeys) {
        const targetValue = target[targetKey];

        let newKey = targetKey;
        let newValue = targetValue;

        const context = {
            key: targetKey,
            value: targetValue,
            row: target
        };

        if (map["*"]) {
            if (map["*"].key !== undefined) {
                newKey = map["*"].key({
                    context,
                    key: targetKey
                });
            }

            if (map["*"].value !== undefined) {
                newValue = map["*"].value({
                    context,
                    value: targetValue
                });
            }
        }

        if (map[targetKey]) {
            if (map[targetKey].key !== undefined) {
                if (typeof map[targetKey].key === "function") {
                    newKey = map[targetKey].key({
                        context,
                        key: targetKey
                    });
                }
                else {
                    newKey = map[targetKey].key;
                }
            }

            if (map[targetKey].value !== undefined) {
                if (typeof map[targetKey].value === "function") {
                    newValue = map[targetKey].value({
                        context,
                        value: targetValue
                    });
                }
                else {
                    newValue = map[targetKey].value;
                }
            }
        }

        resultObject[newKey] = newValue;
    }

    return resultObject;
}