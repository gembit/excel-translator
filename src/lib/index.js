// @flow

import fs from "fs-extra";
import excelStream from "../utils/excel-stream.js";
import { xmlObjectToModel } from "../translators/";
import type { MapObject } from "../translators/";

export type ParsedResults = {
    /**
     * Objects that were correctly parsed
     */
    good: Array<Object>,
    /**
     * Objects that failed on parsing
     */
    bad: Array<Object>
};

/**
 * Parse the given object
 * @returns {Object} parsedObject
 * @param {Object} target 
 */
export function parseObject(target: Object, translator: MapObject): Object {
    return xmlObjectToModel(target, translator);
}

/**
 * Parse the given file
 * @returns {Promise<{ good, bad }>} - An `Object` containing array with parsed Objects(`good`) and array with failed Objects(`bad`)
 * @param {string} filePath 
 */
export async function parseFile(filePath: string, translator: MapObject): Promise<ParsedResults> {
    const fileStream = fs.createReadStream(filePath);
    
    return await parseStream(fileStream, translator);
}

/**
 * Parses the stream
 * @returns {Promise<{ good, bad }>} - An `Object` containing array with parsed Objects(`good`) and array with failed Objects(`bad`)
 * @param {ReadStream} stream 
 */
export function parseStream(stream: Object, translator: MapObject): Promise<ParsedResults> {
    return new Promise((resolve) => {
        const result: ParsedResults = { good: [], bad: [] };

        const xmlStream = excelStream({
            encoding: "utf-8"
        });

        xmlStream.on("data", (data: Object) => {
            try {
                const finalData = parseObject(data, translator);

                result.good.push(finalData);
            }
            catch (exception) {
                throw exception;
                result.bad.push(data);     
            }
        });

        stream.pipe(xmlStream);

        xmlStream.on("end", () => {
            resolve(result);
        });
    });
}