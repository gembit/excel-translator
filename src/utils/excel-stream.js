// https://github.com/dominictarr/excel-stream/blob/master/index.js

let fs       = require("fs");
let os       = require("os");
let path     = require("path");
let chpro    = require("child_process");

let through  = require("through");
let csv      = require("csv-stream");
let osenv    = require("osenv");
let duplexer = require("duplexer");
let concat   = require("concat-stream");

let spawn = chpro.spawn;
if (os.type() === "Windows_NT") spawn = require("win-spawn");

module.exports = function (options) {

    let read = through();

    let duplex;

    let filename = path.join(osenv.tmpdir(), "_"+Date.now());

    let spawnArgs = [];

    if (options) {
        options.sheet && spawnArgs.push("--sheet") && spawnArgs.push(options.sheet) && delete options.sheet;
        options.sheetIndex && spawnArgs.push("--sheet-index") && spawnArgs.push(options.sheetIndex) && delete options.sheetIndex;
    }

    spawnArgs.push(filename);
    
    let write = fs.createWriteStream(filename, { defaultEncoding: "utf-8" })
    .on("close", function () {
        let child = spawn(require.resolve("j/bin/j.njs"), spawnArgs);

        const csvStream = csv.createStream(options);

        csvStream._encoding = "utf-8";

        child.stdout.pipe(csvStream)
        .pipe(through(function (data) {
            let _data = {};
            for (let k in data) {
                k = k.toString("utf-8");
                let value = data[k].toString("utf-8").trim();
                _data[k.trim()] = isNaN(value) ? value : +value;
            }
            this.queue(_data);
        }))
        .pipe(read);
        child.on("exit", function(code) {
            if (code === null || code !== 0) {
                child.stderr.pipe(concat(function(errstr) {
                    duplex.emit("error", new Error(errstr));
                }));
            }
        });
    });

    return (duplex = duplexer(write, read));

};


if (!module.parent) {
    let JSONStream = require("JSONStream");
    let args = require("minimist")(process.argv.slice(2));
    process.stdin
    .pipe(module.exports())
    .pipe(args.lines || args.newlines
      ? JSONStream.stringify("", "\n", "\n", 0)
      : JSONStream.stringify()
    )
    .pipe(process.stdout);
}